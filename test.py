import subprocess

program_path = 'WaveDump.exe'
wavedumpconfig_path = 'WaveDumpConfig_1742.txt'
output_path = 'C:/data/20191029/no-header-512'
number_of_events = 30
autotrigger = False

str_run = f'{program_path} {wavedumpconfig_path} {output_path} {number_of_events} {"Autotrigger" if autotrigger else ""}'
subprocess.run(str_run)