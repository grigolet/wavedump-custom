# Wavedump custom

Usage:

```
path/to/WaveDump.exe <WaveDumpConfig.txt path> <output path> <Number of events> [Autotrigger]
```

Description of paramters:

* `WaveDumpConfigs.txt` path is the path to the configs that should be used. This allows to call 
    different instances of the program with different config files and run multiple digitzers at the same time
* `<output path>` is the path where the wave*.txt files should be saved. The path must be existing or an error will be thrown
* `<Number of events>` is the number of events required to acquire and write to the wavefiles before exiting the program
* `[Autotrigger]` is an optional parameter. If a string is present as fifth parameters (argc == 5) then the acquisition will
    run as if the 'T' key was pressed, i.e. in continuous trigger mode
